$(document).ready(function(event){
    $('button').on('click', function(event){
        event.preventDefault();
        var url_da_api = "https://icuff17-api.herokuapp.com/teachers";
        var id_professor = $('input').val();
        var url_professor = url_da_api + '/' + id_professor;
        var requisicao_professor = $.ajax({
            url: url_professor
        });
        requisicao_professor.done(function(data) {
            var foto_professor = data.photo;

            $('#divprofessor').append('<div class="professor"><h2>' + data.name +'</h2><p>' + data.subject + '</p><img src="'+ foto_professor +'"></div>');
        });
    });
});
